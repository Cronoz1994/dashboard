import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AppRoutingModule} from '../app-routing.module';
import {DashboardMainComponent} from './dashboard-main/dashboard-main.component';
import {DashboardHeaderComponent} from './dashboard-header/dashboard-header.component';
import {DashboardBodyComponent} from './dashboard-body/dashboard-body.component';
import {DashboardMenuListComponent} from './dashboard-menu-list/dashboard-menu-list.component';
import {DashboardMenuItemComponent} from './dashboard-menu-item/dashboard-menu-item.component';
import {DashboardNotFoundComponent} from './dashboard-not-found/dashboard-not-found.component';
import {DashboardNavigationComponent} from './dashboard-navigation/dashboard-navigation.component';

@NgModule({
  declarations: [
    DashboardMainComponent,
    DashboardHeaderComponent,
    DashboardBodyComponent,
    DashboardMenuListComponent,
    DashboardMenuItemComponent,
    DashboardNotFoundComponent,
    DashboardNavigationComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  exports: [
    DashboardMainComponent
  ]
})
export class DashboardModule {
}
