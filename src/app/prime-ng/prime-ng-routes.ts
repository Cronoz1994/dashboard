import {Routes} from '@angular/router';

import {PrimeNgMainComponent} from './prime-ng-main/prime-ng-main.component';
import {PrimeNgDataComponent} from './prime-ng-data/prime-ng-data.component';
import {PrimeNgListComponent} from './prime-ng-list/prime-ng-list.component';
import {PrimeNgDetailsComponent} from './prime-ng-details/prime-ng-details.component';

export const PRIME_NG_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: PrimeNgMainComponent,
    children: [
      {
        path: '',
        redirectTo: 'data'
      },
      {
        path: 'data',
        component: PrimeNgDataComponent,
        data: {title: 'Laboratorio 3', content: 'This data was recived from router and it is working!!!'},
      },
      {
        path: 'list',
        component: PrimeNgListComponent,
        children: [
          {
            path: ':id',
            component: PrimeNgDetailsComponent
          }
        ]
      }
    ]
  }
];
