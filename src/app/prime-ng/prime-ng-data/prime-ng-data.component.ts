import {ActivatedRoute} from '@angular/router';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-prime-ng-data',
  templateUrl: './prime-ng-data.component.html',
  styleUrls: ['./prime-ng-data.component.scss']
})
export class PrimeNgDataComponent implements OnInit, OnDestroy {
  public title: string;
  public content: string;

  private _loadRouteDataSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute) {
    this._loadRouteDataSubscription = new Subscription();
    this.content = 'loading...';
    this.title = 'loading...';
  }

  ngOnInit() {
    this._initialize();
  }

  ngOnDestroy(): void {
    this._loadRouteDataSubscription.unsubscribe();
    this._loadRouteDataSubscription = null;
  }

  private _initialize(): void {
    this._loadRouteDataListener();
  }

  private _loadRouteDataListener(): void {
    this._loadRouteDataSubscription = this._activatedRoute.data
      .subscribe((data: { title: string, content: string }) => {
        this.title = data.title;
        this.content = data.content;
      });
  }

}
