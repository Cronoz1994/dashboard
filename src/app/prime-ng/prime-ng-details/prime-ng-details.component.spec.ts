import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeNgDetailsComponent } from './prime-ng-details.component';

describe('PrimeNgDetailsComponent', () => {
  let component: PrimeNgDetailsComponent;
  let fixture: ComponentFixture<PrimeNgDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimeNgDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeNgDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
