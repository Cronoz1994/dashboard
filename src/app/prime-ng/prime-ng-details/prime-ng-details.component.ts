import {ActivatedRoute, ParamMap} from '@angular/router';
import {Component, OnInit} from '@angular/core';

import {PrimeNgListService} from '../prime-ng-list/prime-ng-list.service';

@Component({
  selector: 'app-prime-ng-details',
  templateUrl: './prime-ng-details.component.html',
  styleUrls: ['./prime-ng-details.component.scss']
})
export class PrimeNgDetailsComponent implements OnInit {
  public component: { id: number, name: string, description: string };

  constructor(private _primeNgListService: PrimeNgListService,
              private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this._activatedRoute.paramMap
      .subscribe((params: ParamMap) => {
        const id = params.get('id');

        this.component = this._primeNgListService.getById(+id);
      });
  }

}
