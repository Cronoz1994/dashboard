import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PrimeNgRoutingModule} from './prime-ng-routing.module';

import {PrimeNgMainComponent} from './prime-ng-main/prime-ng-main.component';
import {PrimeNgDataComponent} from './prime-ng-data/prime-ng-data.component';
import {PrimeNgListComponent} from './prime-ng-list/prime-ng-list.component';
import { PrimeNgDetailsComponent } from './prime-ng-details/prime-ng-details.component';

@NgModule({
  declarations: [
    PrimeNgMainComponent,
    PrimeNgDataComponent,
    PrimeNgListComponent,
    PrimeNgDetailsComponent
  ],
  imports: [
    CommonModule,
    PrimeNgRoutingModule
  ]
})
export class PrimeNgModule {
}
