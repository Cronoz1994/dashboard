import { Component, OnInit } from '@angular/core';
import {PrimeNgListService} from "./prime-ng-list.service";

@Component({
  selector: 'app-prime-ng-list',
  templateUrl: './prime-ng-list.component.html',
  styleUrls: ['./prime-ng-list.component.scss']
})
export class PrimeNgListComponent implements OnInit {
  public componentList: { id: number, name: string, description: string } [];

  constructor(private _primeNgListService: PrimeNgListService) {
    this.componentList = [];
  }

  ngOnInit() {
    this._initialize();
  }

  private _initialize(): void {
    this.componentList = this._primeNgListService.components;
  }

}
