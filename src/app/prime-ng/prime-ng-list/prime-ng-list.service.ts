import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PrimeNgListService {

  private _components: { id: number, name: string, description: string } [];

  constructor() {
    this._components = [
      {
        id: 1,
        name: 'Component 1',
        description: 'Description component 1.'
      },
      {
        id: 2,
        name: 'Component 2',
        description: 'Description component 2.'
      },
      {
        id: 3,
        name: 'Component 3',
        description: 'Description component 3.'
      },
      {
        id: 4,
        name: 'Component 4',
        description: 'Description component 4.'
      }
    ];
  }

  public getById(id: number): { id: number, name: string, description: string } {
    let response: { id: number, name: string, description: string } = null;

    for (let component of this._components) {
      if (component.id === id) {
        response = component;
      }
    }

    return response;
  }

  get components(): { id: number, name: string, description: string }[] {
    return this._components;
  }
}
