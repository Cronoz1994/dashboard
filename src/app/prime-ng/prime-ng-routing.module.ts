import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {PRIME_NG_ROUTES_CONFIG} from './prime-ng-routes';


@NgModule({
  imports: [RouterModule.forChild(PRIME_NG_ROUTES_CONFIG)],
  exports: [RouterModule]
})
export class PrimeNgRoutingModule {
}
